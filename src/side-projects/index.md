---
title: Side Projects
#home: true
layout: Full
projects: 
  - name: listMe
    href: https://listme.chat
    emoji: 📋
    image: listme.jpg
    description: listMe is a Slack add in used by teams all over the world. Keep lists of to-dos, discussion topics, or ideas with each person you chat with in Slack!

  - name: CFPs.dev
    href: https://cfps.dev
    emoji: 🎤
    image: cfpsdotdev.jpg
    description: Track open calls for proposal, submissions, and your talk abstract library all in one place.  Built on Netlify, Netlify Functions, and Fauna DB.

  - name: Build Inclusive.com
    href: https://buildinclusive.com
    emoji: 🦾
    image: buildinclusive.jpg
    description: We need to be conscious of building inclusive from the beginning, work against our unconscious biases, and build with (not just 'for') everyone. That will create a world where everyone can contribute.

  - name: Onboarding Buddy
    href: https://onboardingbuddy.com
    emoji: 🧑‍🤝‍🧑
    image: onboardingbuddy.jpg
    description: Stress free onboarding for any size team. Provide context, boost productivity. Plan, design, and automate the new hire experience.

  - name: Knight Check
    href: https://knightcheck.com
    image: knightcheck.jpg
    emoji: ✅
    description: Protect your realm with daily COVID-19 health screening. Includes tools for school administrators and parents, including screening questions, arrival badges, and management of exceptions.

  - name: Quiz Me
    href: https://getquizme.com
    image: quizme.jpg
    emoji: 📇
    description: Flashcards that actually work. Unlock the power of Spaced Repetition, known for years as the best way to ensure long-term retention of information and knowledge.

  - name: Do You Even Art?
    href: https://doyouevenart.com
    image: doyouevenart.jpg
    emoji: 🧑‍🎨
    description: The podcast about art and its place in pop culture in the 21st century.

  - name: HN Suite
    href: https://hnsuite.com
    image: hnsuite.jpg
    emoji: 🟧
    description: An open source, unofficial extension to make Hacker News a little nicer. 

  - spacer: true
---

<div class="proj-container">
    <h1>Side Projects</h1>
    <div class="project-cards">
        <div class="project" v-for="proj in $page.frontmatter.projects">
            <div class="project-box" v-if="!proj.spacer">
                <a :href="proj.href" target="_blank">
                    <img :src="`/img/projects/${proj.image}`" :alt="proj.name" style="width:100%" v-if="proj.image" />
                    <div class="project-content">
                        <p>
                            {{proj.description}}
                        </p>
                    </div>
                </a>
            </div>
            <div v-else-if="proj.spacer">
                <span>&nbsp;</span>
            </div>
        </div>
    </div>
</div>

<style scoped>
a {
  color: inherit;
}

h2 {
  border-bottom: none;
  margin-block-start: 0;
}

.proj-container {
  margin: 2rem;
}

.project-cards {
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;
}

.project {
  width: 30%;
  transition: 0.3s;
  margin: 1rem;
}

.project-box {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
}

.project-content {
    padding: 1rem;
}

.project-box:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.33);
}

.project:hover h2, .project:hover p {
  color: #3F51B5;
}

.project:hover h2 .emoji {
  animation: spinit 1s linear;
}

@media (max-width: 1200px) {
    .proj-container {
        margin: 0;
    }
    .project {
        width: 45%
    }
}

@media (max-width: 719px) {
    .proj-container {
        margin: 0;
    }
    .project {
        width: 100%;
    }
}
</style>