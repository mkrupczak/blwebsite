---
title: Brendan O'Leary | Domains
---

# Domains I own

This is a list of domains I own or own a part of (as part of [Liscio Apps](https://liscioapps.com/)). The list is mainly to keep myself honest about all the domains I own and if I'm actually using them.

| #   | Domain                | In use? | Notes                                                                                                                              |
| --- | --------------------- | ------- | ---------------------------------------------------------------------------------------------------------------------------------- |
| 1   | boleary.dev           | Yes     | You're on it!                                                                                                                      |
| 2   | brendan.fyi           | Yes     | Used as a [URL shorter](https://gitlab.com/brendan/.fyi)                                                                           |
| 3   | brendanoleary.com     | Yes     | Just redirects to boleary.dev                                                                                                      |
| 4   | olearycrew.com        | Yes     | Just redirects to boleary.dev.  Used for email                                                                                     |
| 5   | doyouevenart.com      | Yes     | Podcast I co-host about art in modern times.  Currently on hiatus but still proud of what's there 😄                               |
| 6   | hnsuite.com           | Yes     | Site for a Chrome extension I wrote to make reading Hacker News a little easier and nicer                                          |
| 7   | definotily.com        | Yes     | Just a funny word someone said once, so I made it say "No, yeah no"                                                                |
| 8   | buildinclusive.com    | Yes     | Website that goes along with my talk on inclusive and accessible design                                                            |
| 9   | 7rings.dev            | No      | I do use this every once and a while...it's based off the Ariana Grande based development plan                                     |
| 10  | allremotestack.com    | Yes     | A site about the stack of software you need to be a Remote company                                                                 |
| 11  | gitlabtheme.com       | Yes     | A list of various GitLab-themed themes                                                                                             |
| 12  | labwork.dev           | Yes     | Various "side" projects I use at work                                                                                              |
| 13  | tanukigotchi.com      | Yes     | A fun game based off the old school Tamagotchi                                                                                     |
| 14  | liscioapps.com        | Yes     | Homepage for Liscio Apps                                                                                                           |
| 15  | listme.chat           | Yes     | Side project - a Slack app                                                                                                         |
| 16  | getquizme.com         | Yes     | Side project - a spaced repetition app                                                                                             |
| 17  | knightcheck.com       | Yes     | Side project - COVID screening tool for schools                                                                                    |
| 18  | onboardingbuddy.com   | Yes     | Side project - Slack based onboarding app                                                                                          |
| 19  | retutora.com          | No      | Dead side project that was going to be the AirBnB of finding tutors                                                                |
| 20  | musicteacher.io       | No      | Dead side project - app for independent music teachers                                                                             |
| 21  | emailishard.com       | No      | Dead side project - Give your phone number, get your email address                                                                 |
| 22  | askjocko.com          | No      | Silly project where you as Jocko questions, he tells you to do pull ups                                                            |
| 23  | slashcollartug.com    | No      | Silly Slack app that just adds `/collartug` to Slack and returns [this gif](https://media.giphy.com/media/WRMq4MMApzBeg/giphy.gif) |
| 24  | getthedamnvaccine.com | Yes     | Stories of people who didn't get the vaccine and lived (or died) to regret it.                                                     |
| 25  | cfps.dev              | Yes     | App to track calls for papers (submitting talks to conferences)                                                                    |
| 26  | slackagemanager.com   | No     | Failed slack package manager for emojishttps://gitlab.com/brendan/brendan/-/issues/35                                                     |
| 27  | getsimpletodo.com   | Yes     | Simple todo list app for Slack                                                    |
| 28  | crewmail.xyz   | Yes     | Email proxy domain                                    |
| 29  | gdanskcontribute.com   | Yes     | An inside joke.  Yes a whole domain for an inside joke                    |
| 30  | learningtheatretech.com  | Yes     | A blog for learning about technical theater           |
| 31  | nottechnicalenough.com   | Yes     | A blog to encourage people to contribute, speak, etc. even if they don't feel technical enough    |



## Subdomains and projects
| #   | Domain               | In use? | Notes                                                                                        |
| --- | -------------------- | ------- | -------------------------------------------------------------------------------------------- |
| 1   | class-timer.web.app  | Yes     | A class timer to keep Orange Fitness folks who may be hard of hearing in sync with the class |
| 2   | leon.boleary.dev     | Yes     | Some "N O E L" letters to rearrange with friends                                             |
| 3   | timeline.boleary.dev | Yes     | Links to my polywork profile                                                                 |
