---
title: "My epiphany about the protests"
date: 2021-01-08 02:00:00
image: /img/blog/element5-digital-T9CXBZLUvic-unsplash.jpg
type: post
blog: true
author: brendan
excerpt: > 
 Arguing that Congress can reject electoral college votes it doesn't like isn't arguing for the sanctity of elections - it's arguing for the abolition of elections.
meta:
  - name: 'twitter:title'
    content: 'My epiphany about the protests'
  - name: 'twitter:description'
    content: 'Arguing that Congress can reject electoral college votes is not arguing for the sanctity of elections - it is arguing for the abolition of them.'
  - name: 'og:title'
    content: 'My epiphany about the protests'
  - name: 'og:description'
    content: 'Arguing that Congress can reject electoral college votes is not arguing for the sanctity of elections - it is arguing for the abolition of them.'
---

I want to be precise. This isn't from a place of hyperbole - it is in line with the interpretation that Republicans and Democrats alike expressed in Congress.

Anyone who protested on Wednesday, peaceful or not, and desired Congress to reject the votes from a state where their candidate didn't win wasn't protesting inside of our accepted system of government. **They were protesting instead for Congress to overthrow the rule of law, the people's will, and most critically, the Constitution itself.**

We can have disagreements about President Trump. We can have debates about policy matters.  But most of the time, those are within the bounds of our system of government.  However, any attempt or protest desiring for Congress to object to and reject the ballots from any state is *outside* our system of government.

In our representative democracy, this is how voting for President works:
* The people vote for President and Vice President in their state
* Actually, they are voting for "electors" to select those people as winning the state's votes in the Electoral College
* The electors from each state meet and send those votes to Congress
* Congress opens the votes, confirms they are authentic, and verifies the winner

In the laws governing elections, Congress can challenge and object to electors from the various states.  The basis for this had its root in the 1860s when some states were in full revolt against the country, and many sent TWO sets of electors to Congress.  In that case, there needed to be a procedure to determine which electors were the right and lawful ones.

This objection process is covered by [Section 3 of the US code](https://www.law.cornell.edu/uscode/text/3/15).  It is dense but gives the whole process of reading the electoral votes.  In particular, though, let's read this line:

>  no electoral vote or votes from any State which shall have been regularly given by electors whose appointment has been lawfully certified to according to section 6 of this title from which but one return has been received shall be rejected

Congress is not deciding if they agree with the election.  They are not deciding if they are okay with the way the state conducted the election.  They are not deciding if they like mail-in ballots or not.  Heck - the state didn't even have to have an election!  If they decided that they were going to pick their electors by random draw on the nightly news, that would be acceptable if it was "in such a Manner as the Legislature" of that state directed (as per Article II Section 1 of the Consitution):

> Each State shall appoint, in such Manner as the Legislature thereof may direct, a Number of Electors, equal to the whole Number of Senators and Representatives to which the State may be entitled in the Congress

If, as the objectors in Congress attempted and the protestors I must assume supported, Congress were to reject the votes of any state - knowing that that state sent those electors - then it would be the end of American self-rule.

That again is not hyperbolic.  There would be no need to have an election for President and Vice President.  All that would matter is control of Congress.  Because if Congress has the unilateral ability to reject votes they don't like, then whoever is in power in Congress at the time of the election could pick and choose which states to count and which not to count and pick their candidate as the winner.

This is what is so shocking to conservatives who don't support this effort.  It is the conservative position, the Republican position (both big and little "r") that the states have the sole power to elect the President and Vice President.  This is central to how our system of government works.  It's central to how we've decided to give everyone a voice in this country.  It's central to what the founders saw for our nation.  

The place to dispute any issue you have with an election is not in Congress - it's in the states. The proper forum for elected officials to raise concerns about fair elections is through the states and through the courts; that work was already done, in more than 60 cases.  But honestly that actually doesn't matter.  I don't care in this case if you agree or disagree with how the states conducted their election - it doesn't matter.  It may matter for who you supported in this election, but if we decide that one branch of federal government gets to decide who is President...then your vote won't ever count again.

Arguing that Congress can reject electoral college votes it doesn't like isn't arguing for the sanctity of elections - it's arguing for the abolition of elections.
