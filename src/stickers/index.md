# Stickers Across the World

My goal is to send GitLab stickers to as many countries as possible.  If your country isn't yet represented here - send me a DM on [Twitter](https://twitter.com/olearycrew) and I'll try and get you some!

<iframe id="stickersFram"
    title="Stickers Frame"
    width="750"
    height="700"
    src="https://brendan.gitlab.io/stickers/">
</iframe>

[Stickers source code](https://gitlab.com/brendan/stickers)
