---
title: Black Mirror Season 5 - DevOps
talk: true
tagline: "We need to work carefully to prevent the next season of Black Mirror from featuring SRE, DevOps engineer, or CISO characters."
abstract: |
    Black Mirror presents a haunting view of how modern technology places society a “minute away” from a dystopian future. DevOps and those of us that practice it find ourselves in a similar situation - partially mature technologies whose implications we don’t yet fully understand. Heartbleed, Equifax and now Meltdown & Spectre can make us feel like there is no escaping this dark future. But just as Black Mirror examines the extremes of these concepts as a canary in the mine shaft for society, we too can carefully employ practices that will prevent season 5 from featuring Site reliability engineer, DevOps engineer, or CISO characters.

    In this talk, we'll learn how to use the powerful concepts and tools behind DevOps for good... with great power comes great responsibility....but also great opportunity to do good for our businesses, each other and our world. By working together with product, business, and external teams; embedding security into how we operate; and measuring everything we do we can empower our teams to thrive. 
video: https://www.youtube.com/embed/POBAnk3nWos
events:
  - name: DevOps Day Baltimore 2018
    date: "2018-03-22"
    href: https://www.youtube.com/watch?v=POBAnk3nWos&feature=emb_title
---

<Talk :talk="$page.frontmatter"/>

