---
title: "GitLab CI/CD: A Love Story"
talk: true
tagline: The ability to reliably and repeatable build, test, deploy review applications and deploy to production - is critical for collaborating and delivering better software faster.
abstract: |
    For many years, we lived in a world where it was hard for an average engineering organization to have truly professional-grade tools and processes to build and release software.  The industry reaction to that was an explosion of tools that, in some ways, make it more confusing when trying to understand what your team should spend their time on.  You want your team to spend their time delivering value for your customers, not stitching together different tools.
    
    However, understanding the best practices around CI and CD learned over the past decade is critical for large-scale production applications. That is - the ability to reliably and repeatable build, test, deploy review applications and deploy to production - is critical for collaborating and delivering better software faster. 
    
    In this talk, we'll spend some time going over the lessons learned, and then use that understanding to explore what how GitLab CI/CD came to be.  And how a single application for your entire DevOps lifecycle can get your team back to what they do best - engineering amazing software for your customers.
slides: 'https://docs.google.com/presentation/d/e/2PACX-1vQ8y6Tg3naQELjWQLLCFONS6g3-Q16EHDenbYdYVi06dwnr3EdIGZTnpgK2Gau3MmGt2GY3rqggs22d/embed'
video: ''
events: []

---

<Talk :talk="$page.frontmatter"/>
