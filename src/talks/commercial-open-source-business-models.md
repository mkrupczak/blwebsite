---

title: "Commercial Open Source Business Models"
subtitle: "GitLab’s Bet on Buyer-based Open Core"
talk: true
tagline:
  GitLab has made a bet on what we call buyer-based open core, and so far,
  it's paid off. However, this wasn't luck or some special sauce. It took many
  iterations on the GitLab business model to end up where we are today.
abstract: |
  In the age of commoditization, hyper-cloud service wrapping, and increased competition in the open-source world, many Commerical Open Source (COSS) companies are struggling to find a sustainable business model. Balancing support of open-source with the need to maintain the business that provides that support can cause companies to make rash decisions about licensing and their business models.

  In this talk, we'll take a look at the commercial open-source landscape. We will learn from those who have been successful in building sustainable open-source communities and businesses. We will explore the various business models available to open source-based companies and where there have been successes and failures. We will use the case study of GitLab, a complete DevOps platform that is open core and based on git. The company has grown 177% year over year, even with the growing uncertainty in the space.

slides: "https://docs.google.com/presentation/d/e/2PACX-1vS9HcaPxDwOYT3fOpyRyJFFi3hoHOii03oDqcwv-h16bTGwZ7aOcbIx2UZp4abD9KVnMhD_OBKNS-Op/embed"
video: ""
events: 
 - name: All Things Open
   date: "2020-10-20"
---

<Talk :talk="$page.frontmatter"/>
