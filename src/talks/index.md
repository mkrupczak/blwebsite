---
title: Talks
layout: Talks
---

## My Talks

For a list of all of my abstracts, see [my abstracts](https://cfps.dev/u/brendan)

See my [events](https://cfps.dev/u/brendan/events) page for upcoming events. I even have what events I've submitted talks to buy may or may not be accepted to in the spirt of [transparency](https://about.gitlab.com/handbook/values/#transparency). You can also check out my [speaker bio](/talks/bio/).

## Previous Talks

[List of all of my speaking engagements](https://cfps.dev/u/brendan/events). You can also see a collection of many of my previous talks [on this YouTube playlist](https://www.youtube.com/playlist?list=PLHPUDUbFkW1bJOcOrMxpW6hnIHLOvThf2).
