---
title: Kubernetes is not DevOps
talk: true
tagline: Kubernetes is an amazing platform.  But it does not solve all the world's problems.  In fact, it doesn't even solve your DevOps problems.
abstract: |
    Kubernetes is an amazing platform.  It truly is a once-in-a-generation change to the way we think about building and deploying software.  The history books will treat the advent of Kubernetes similarly to the advent of such foundational technologies like Linux, open-source software, containers, and the cloud.  But how will history view those of us who are using Kubernetes?  For many in our industry, Kubernetes has become the proverbial "hammer" to every "nail" that is in sight.  Maybe you've seen CIOs who think saying "Kubernetes" three times in front of a mirror will suddenly allow them to close their technology gaps with their competitors. 

    In this talk, we'll talk about what Kubernetes is (like every other tech talk for the past few years).  But more importantly, we'll focus on what Kubernetes *isn't*.  While Kubernetes and the technologies surrounding it promises to solve some fundamental problems that many of us struggle with, we cannot "live on Kubernetes alone."  Looking at other tectonic shifts in software development, we'll learn from the triumphs and mistakes of the past to plot a course for the future.  Spoiler alert: you don't need to go train you're entire engineering staff on Kubernetes and YAML.

# Originally based on https://drive.google.com/file/d/1aB5NrAck8GdyyO3Q8LzllUTJwSZFaFB8/view?ths=true 
#slides: https://docs.google.com/presentation/
#video: https://www.youtube.com/
#events:
#  - name: A Great Event
#    date: "2020-02-02"
#    href: https://www.youtube.com/
---

<Talk :talk="$page.frontmatter"/>


