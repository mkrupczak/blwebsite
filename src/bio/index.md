---
title: "Brendan O'Leary Speaker Bio"
---

# Brendan's Speaker Bio & Media Resources

> NOTE: this page is mostly for me.
>
> It is so that I remember where I have all of these things when I need it. No one else really cares about having all of my headshots...I don't think 😃

## Flexible Bio

<Bio len="medium"/>

## Standard Bio

<Bio len="long" :showBtns="false"/>

## Media

### Profile Images

<table>
    <tr>
        <td>
            <a href="/img/instagram.jpg" target="_blank">
                <BlogImage image="/img/instagram.jpg" caption="Thinking pensively about DevOps" />
            </a>
        </td>
        <td>
            <a href="/img/headshots/stairs.jpg" target="_blank">
            <BlogImage image="/img/headshots/stairs.jpg" caption="2021 Headshot" />
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="/img/speaker02.png" target="_blank">
                <BlogImage image="/img/speaker02.png" caption="Speaking at GitLab Contribute in New Orleans" />
            </a>
        </td>
        <td>
            <a href="/img/brendan2020.jpg" target="_blank">
            <BlogImage image="/img/brendan2020.jpg" caption="2020 Headshot" />
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <BlogImage image="/img/headshot2.png" caption="Speaking at DevOps Days Baltimore" />
        </td>
        <td>
            <a href="/img/headshot.png" target="_blank">
            <BlogImage image="/img/headshot.png" caption="Older headshot...more hair" />
            </a>
        </td>
    </tr>
    <tr>
        <td>
            <a href="/img/brendan_avatar.png" target="_blank">
            <BlogImage image="/img/brendan_avatar.png" caption="Avatar with transparent backgrond" />
            </a>
        </td>
        <td>
            <a href="/img/brendan_avatar_blue.png" target="_blank">
            <BlogImage image="/img/brendan_avatar_blue.png" caption="Avatar with blue backgrond" />
            </a>
        </td>
    </tr>
</table>

### Streaming Media

<table>
    <tr>
        <td>
            <BlogImage image="/img/twitch-frame.png" caption="Twitch streaming frame" />
        </td>
        <td>
            <BlogImage image="/img/tanuki-color-outline.png" caption="Color outline tanuki" />
        </td>
    </tr>
</table>
