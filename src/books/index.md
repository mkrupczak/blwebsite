---
title: Books & Media
---

# Books I ❤️

## Software

* [Joel on Software](https://amzn.to/3vkOp28)
* [Accelerate: The Science of Lean Software and DevOps](https://amzn.to/34dwtum) by Nicole Forsgren, Ph.D., Jez Humble, and Gene Kim
* [The Developers Guide to Content Creation](https://www.developersguidetocontent.com/) by [Stephanie Morillo](https://twitter.com/radiomorillo)

## Business 

* The [GitLab Handbook](https://about.gitlab.com/handbook/)
* [The Hard Thing About Hard Things](https://amzn.to/3yCig81) by Ben Horowitz
* [How to Win Friends & Influence People](https://amzn.to/2QUCsBg)
* Malcolm Gladwell's [The Tipping Point](https://amzn.to/3oRiLqE)
* [Extreme Ownership](https://amzn.to/3vof7Xv): How U.S. Navy SEALs Lead and Win

## Fiction 

- [The Soul of a New Machine](https://amzn.to/3bVV62K) by Tracy Kidder
- [Dune](https://amzn.to/3ywxxau) by Frank Herbert
- [Zen and the Art of Motorcycle Maintenance](https://amzn.to/3bVV62K)  by Robert M. Pirsig

## Other

* [The Man who Invented the Computer](https://amzn.to/3fmtGp7) by Jane Smiley
* [Mere Christianity](https://amzn.to/34ibMxq) by C. S. Lewis
