> Note: The default branch is called `main`

From [VuePress Blog Boilerplate](https://github.com/bencodezen/vuepress-blog-boilerplate) by bencodezen

Heavily modified by me (Brendan)

## Reminders

- Images & static content are stored in: `src/.vuepress/public`
